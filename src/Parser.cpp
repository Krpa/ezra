#include "Parser.h"
#include "bioparser/bioparser.hpp"
#include "Sequence.h"
#include "Extension.h"
#include "globals.h"

#include <unordered_set>

namespace ezra {
using namespace std;
extern int chunksize;

void parseProcessFastq(const string& strFastq, MapIdToSeq& mIdToSeq) {
  vector<unique_ptr<Sequence>> aReads;
  auto fastqParser = bioparser::createParser<bioparser::FastqParser, ezra::Sequence>(strFastq);
  fastqParser->parse_objects(aReads, -1);
  for (uint32_t i = 0; i < aReads.size(); i++) {
    mIdToSeq.emplace(aReads[i]->seq_strName, std::move(aReads[i]));
  }
}

// Parse FASTQ and use only reads in the Set readsUsed
// Those should be the ones with valid overlaps
// Also read in chunks of 1GB
void parseProcessFastq(const string& strFastq, MapIdToSeq& mIdToSeq, SetRName& readsUsed) {
  auto fastqParser = bioparser::createParser<bioparser::FastqParser, ezra::Sequence>(strFastq);
  if (ezra::chunksize > 0) {		// Loading in chunks of set size
    vector<unique_ptr<Sequence>> aReads;
    uint64_t max_bytes = ezra::chunksize * 1024*1024;
    bool status = true;
    while (true) {
      status = fastqParser->parse_objects(aReads, max_bytes);
      for (uint32_t i = 0; i < aReads.size(); i++) {
        if (readsUsed.find(aReads[i]->seq_strName) != readsUsed.end()) {
          mIdToSeq.emplace(aReads[i]->seq_strName, std::move(aReads[i]));
        }
      }
      if (status == false) break;		// Break if last loaded chunk was incomplete
    }
  }
  else {		// Loading in one go
    vector<unique_ptr<Sequence>> aReads;
    fastqParser->parse_objects(aReads, -1);
    for (uint32_t i = 0; i < aReads.size(); i++) {
      if (readsUsed.find(aReads[i]->seq_strName) != readsUsed.end()) {
        mIdToSeq.emplace(aReads[i]->seq_strName, std::move(aReads[i]));
      }
    }
  }
}

void parseProcessFasta(const string& strFasta, MapIdToSeq& mIdToSeq) {
  vector<unique_ptr<Sequence>> aReads;
  auto fastaParser = bioparser::createParser<bioparser::FastaParser, ezra::Sequence>(strFasta);

  fastaParser->parse_objects(aReads, -1);
  for (uint32_t i = 0; i < aReads.size(); i++) {
    mIdToSeq.emplace(aReads[i]->seq_strName, std::move(aReads[i]));
  }
}

void parseProcessPafExt(const string& strPaf, MapIdToExt& mIdToExt) {
  vector<unique_ptr<ezra::Extension>> aExtensions;
  auto pafParser = bioparser::createParser<bioparser::PafParser, Extension>(strPaf);
  pafParser->parse_objects(aExtensions, -1);

  // preprocessing - mark contained reads
  unordered_set<string> usContained;
  for (const auto& ext : aExtensions) {
    if (ext->ext_eType == ET_CONTAINED) {
      usContained.emplace(ext->ext_strName);
    }
  }

  for (uint32_t i = 0; i < aExtensions.size(); i++) {
    if (aExtensions[i]->ext_eType != ET_INVALID && usContained.find(aExtensions[i]->ext_strName) == usContained.end()) {
      mIdToExt[{aExtensions[i]->ext_strTarget, aExtensions[i]->ext_eType}].emplace_back(std::move(aExtensions[i]));
    }
  }
}

// Parse PAF and populate the Set readsUsed with the reads that have valid overlaps
// Also, read in chunks of 1GB
void parseProcessPafExt(const string& strPaf, MapIdToExt& mIdToExt, SetRName& readsUsed) {
  // vector<unique_ptr<ezra::Extension>> aExtensions;
  auto pafParser = bioparser::createParser<bioparser::PafParser, Extension>(strPaf);
  unordered_set<string> usContained;
  if (ezra::chunksize > 0) {		// Loading in chunks of set size
    uint64_t max_bytes = ezra::chunksize * 1024*1024;
    bool status = true;
    while (true) {
      vector<unique_ptr<ezra::Extension>> aExtensions;
      status = pafParser->parse_objects(aExtensions, max_bytes);

      // preprocessing - mark contained reads
      // and reads with valid overlaps
      for (const auto& ext : aExtensions) {
        if (ext->ext_eType == ET_CONTAINED) {
          usContained.emplace(ext->ext_strName);
        }
        else {
          readsUsed.emplace(ext->ext_strName);
          readsUsed.emplace(ext->ext_strTarget);
        }
      }

      for (uint32_t i = 0; i < aExtensions.size(); i++) {
        if (aExtensions[i]->ext_eType != ET_INVALID && usContained.find(aExtensions[i]->ext_strName) == usContained.end()) {
          mIdToExt[{aExtensions[i]->ext_strTarget, aExtensions[i]->ext_eType}].emplace_back(std::move(aExtensions[i]));
        }
      }

      if (status == false) break;		// Break if last loaded chunk was incomplete
    }
  }
  else {			// Loading in one go
    vector<unique_ptr<ezra::Extension>> aExtensions;
    pafParser->parse_objects(aExtensions, -1);

    // preprocessing - mark contained reads
    for (const auto& ext : aExtensions) {
      if (ext->ext_eType == ET_CONTAINED) {
        usContained.emplace(ext->ext_strName);
      }
      else {
        readsUsed.emplace(ext->ext_strName);
        readsUsed.emplace(ext->ext_strTarget);
      }
    }

    for (uint32_t i = 0; i < aExtensions.size(); i++) {
      if (aExtensions[i]->ext_eType != ET_INVALID && usContained.find(aExtensions[i]->ext_strName) == usContained.end()) {
        mIdToExt[{aExtensions[i]->ext_strTarget, aExtensions[i]->ext_eType}].emplace_back(std::move(aExtensions[i]));
      }
    }
  }
}

}
