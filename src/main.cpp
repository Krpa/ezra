#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <fstream>
#include <algorithm>
#include <set>
#include <iterator>
#include <unordered_set>

# include <ctime>

#include <getopt.h>

#include "Bridger.h"
#include "globals.h"

using namespace std;
using namespace ezra;

// Global variable that determines whether to use multithreading or not
// Not using multithreading unless specified in the parameters
int ezra::multithreading = 0;

// Global variable that determines chunk size (in MB) for loading read and overlap files
// Contigs are loaded all at once since they are much smaller in size
int ezra::chunksize = 1*1024;

void print_global_variables() {
  std::cerr << "GLOBAL PARAMETERS:" << endl;
  std::cerr << "Use multithreading: " << ezra::multithreading << endl;
  std::cerr << "Chunk size: " << ezra::chunksize << endl;
}

void print_version_message_and_exit() {
  const char* versionmessage = "\nEzra version 0.3!\n";
  std::cerr << versionmessage;
  exit(0);
}

void print_help_message_and_exit() {
  const char* helpmessage = "\nHow to run Ezra :"
    "\nEzra -f (--folder) <Input folder>"
    "\n    or"
    "\nEzra -r <Reads file> -c <Contigs file> -o <Overlaps file>"
    "\n    The program will perform one iteration of the algorithm"
    "\n    and output contigs to the standard output!."
    "\n    <Input foder> must contain the following files:"
    "\n    - reads.fastq - reads in FASTQ/FASTA format"
    "\n    - readsToContigs.paf - overlaps between reads and contigs"
    "\n    - contigs.fasta - contigs in FASTA format"
	"\nIf Reads file, Contigs file or Overlaps are specified,"
	"\nthey will not be looked for in the Input folder"
    "\nOptions:"
    "\n-f (--folder)     specify input folder for Ezra"
    "\n-r (--reads)      specify reads file for Ezra"
    "\n-c (--contigs)    specify contigs file Ezra"
    "\n-o (--overlaps)   specify overlaps file for Ezra"
    "\n-m (--multithreading)   use multithreading, specify number of threads"
    "\n-k (--chunk-size) specify chunk size in MB, for loading read and overlap files"
    "\n                  default chunk size is 1GB"
    "\n                  use -1 for using loading all in one chunk"
    "\n-v (--version)    print program version"
    "\n-h (--help)       print this help message\n";

  std::cerr << helpmessage;
  exit(0);
}

int main(int argc, char **argv)
{

  // KK: Defining basic program nOptions
  const char* const short_opts = "hvr:c:o:f:m:k:";
  const option long_opts[] = {
    {"help", no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'v'},
    {"folder", required_argument, NULL, 'f'},
    {"reads", required_argument, NULL, 'r'},
    {"contigs", required_argument, NULL, 'c'},
    {"overlaps", required_argument, NULL, 'o'},
    {"multithreading", required_argument, NULL, 'm'},
    {"chunk-size", required_argument, NULL, 'k'},
    {NULL, no_argument, NULL, 0}
  };

  int readsSet, contigsSet, overlapsSet;
  readsSet = contigsSet = overlapsSet = 0;
  string strData, strReadsFasta, strMMPaf, strContigsFasta;

  int opt;
  while ((opt = getopt_long(argc, argv, short_opts, long_opts, NULL)) != -1)
    switch(opt) {
    case 'h':
      print_help_message_and_exit();
      break;
    case 'v':
      print_version_message_and_exit();
      break;
    case 'f':
      readsSet = contigsSet = overlapsSet = 1;
      strData = optarg;
      strReadsFasta = strData + "reads.fastq";
      strMMPaf = strData + "readsToContigs.paf";
      strContigsFasta = strData + "contigs.fasta";
      break;
    case 'r':
      readsSet = 1;
      strReadsFasta = optarg;
      break;
    case 'c':
      contigsSet = 1;
      strContigsFasta = optarg;
      break;
    case 'o':
      overlapsSet = 1;
      strMMPaf = optarg;
      break;
    case 'm':
      ezra::multithreading = std::stoi(optarg);
      break;
    case 'k':
      ezra::chunksize = std::stoi(optarg);
      break;
    }

  if (argc < 2 || readsSet == 0 || contigsSet == 0 || overlapsSet == 0) {
     std::cerr << "\nNot all arguments specified!";
     print_help_message_and_exit();
  }

  print_global_variables();

  std::time_t start_time = std::time(nullptr);
  std::cerr << "\nEZRA: Starting Ezra: " << std::asctime(std::localtime(&start_time)) << start_time << " seconds since the Epoch";

  std::time_t current_time = std::time(nullptr);
  std::cerr << "\nEZRA: Finished loading: " << std::asctime(std::localtime(&current_time)) << (current_time - start_time) << " seconds since the start\n";

  ezra::Bridger bridger(strReadsFasta, strMMPaf, strContigsFasta);

  current_time = std::time(nullptr);
  std::cerr << "\nEZRA: Finished initializing bridger: " << std::asctime(std::localtime(&current_time)) << (current_time - start_time) << " seconds since the start\n";

  bridger.Execute();

  current_time = std::time(nullptr);
  std::cerr << "\nEZRA: Finished Ezra run: " << std::asctime(std::localtime(&current_time)) << (current_time - start_time) << " seconds since the start\n";

  return 0;
}
