#pragma once

#include <map>
#include <vector>
#include <memory>
#include <string>
#include <set>

namespace ezra {
  class Sequence;
  class Extension;

  using MapIdToExt = std::map<std::pair<std::string, uint32_t>, std::vector<std::shared_ptr<Extension>>>;
  using MapIdToSeq = std::map<std::string, std::shared_ptr<Sequence>>;
  using SetRName = std::set<std::string>;

}
