#include "Types.h"

namespace ezra {

extern void parseProcessFastq(const std::string& strFastq, MapIdToSeq& mIdToSeq);
extern void parseProcessFastq(const std::string& strFastq, MapIdToSeq& mIdToSeq, SetRName& readsUsed);
extern void parseProcessFasta(const std::string& strFasta, MapIdToSeq& mIdToSeq);
extern void parseProcessPafExt(const std::string& strPaf, MapIdToExt& mIdToExt);
extern void parseProcessPafExt(const std::string& strPaf, MapIdToExt& mIdToExt, SetRName& readsUsed);

}
