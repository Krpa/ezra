#include "Graph.h"
#include <algorithm>
#include <memory>
#include <map>
#include <vector>
#include <unordered_set>
#include <iostream>

#include "Extension.h"
#include "Sequence.h"
#include "globals.h"

#include "spoa/spoa.hpp"
#include "thread_pool/thread_pool.hpp"

namespace ezra {

using namespace std;
extern int multithreading;

Edge::Edge(string strX, string strY, ExtensionType eX, ExtensionType eY, const vector<string>& aBridge) :
    strX(strX), strY(strY), eX(eX), eY(eY), ulCoverage(aBridge.size()) {
  if (aBridge.size() > 0) {
    strBridgeRead = aBridge[aBridge.size() - 1];
  }
}

// KK: Nothing to go here, just testing a regular constructor
ScaffoldExtension::ScaffoldExtension(string name, string seq, bool bRCSuffix,
	                                 string strSfxId, bool bRCPrefix, string strPfxId) :
    name(name), seq(seq), bRCSuffix(bRCSuffix), strSfxId(strSfxId), bRCPrefix(bRCPrefix), strPfxId(strPfxId) {}


vector<string> CalculateBridgeCoverage(vector<shared_ptr<ezra::Extension>> aExtX,
  vector<shared_ptr<ezra::Extension>> aExtY, bool bOrientation) {
  vector<string> aBridge;
  for (const auto itX : aExtX) {
    for (const auto itY : aExtY) {
      if (itX->ext_strName == itY->ext_strName &&
            (itX->ext_bOrientation == itY->ext_bOrientation) == bOrientation) {
          aBridge.push_back(itX->ext_strName);
      }
    }
  }
  return aBridge;
}

uint32_t IsContigCircular(const string& strContigId, MapIdToExt& mSeqToExt) {
  return CalculateBridgeCoverage(mSeqToExt[{strContigId, ET_SUFFIX}], mSeqToExt[{strContigId, ET_PREFIX}], true).size();
}

/*
 * KK: Accepts contig names and a map connecting contigs to overlaps (extension)
 *     Returns a vector of Edges that are used for bridging contigs 
 *    Each bridging edge contains only one read (the longest one)
 */
vector<Edge> CreateChains(const vector<string>& aContigIds, MapIdToExt& mSeqToExt) {
  vector<Edge> aEdges;
  uint32_t ulContigsN = aContigIds.size();
  for (uint32_t i = 0; i < ulContigsN; i++) {
    cerr << aContigIds[i] << " pre " << mSeqToExt[{aContigIds[i], ET_PREFIX}].size() << " suf " << mSeqToExt[{aContigIds[i], ET_SUFFIX}].size() << endl;
    // skip circular contigs
    uint32_t ulPreSufBridge = IsContigCircular(aContigIds[i], mSeqToExt);
    if (ulPreSufBridge > 0) {
      cerr << "circular " << aContigIds[i] << " intersection " << ulPreSufBridge << endl;
      continue;
    }
    for (uint32_t j = i + 1; j < ulContigsN; j++) {
      if (IsContigCircular(aContigIds[j], mSeqToExt) > 0) {
        continue;
      }
      for (auto eTypeI : {ET_SUFFIX, ET_PREFIX}) {
        for (auto eTypeJ : {ET_SUFFIX, ET_PREFIX}) {
          vector<string> aBridge = CalculateBridgeCoverage(mSeqToExt[{aContigIds[i], eTypeI}], mSeqToExt[{aContigIds[j], eTypeJ}], eTypeI != eTypeJ);
          if (aBridge.size() > 0) {
            cerr << aContigIds[i] << " " << StringFromExtensionType(eTypeI) << " " << aContigIds[j] << " " << StringFromExtensionType(eTypeJ) << " " << aBridge.size() << endl;
            aEdges.emplace_back(aContigIds[i], aContigIds[j], eTypeI, eTypeJ, aBridge);
          }
        }
      }
    }
  }

  sort(aEdges.begin(), aEdges.end(), [](const Edge& lhs, const Edge& rhs) { return lhs.ulCoverage > rhs.ulCoverage; });
  map<string, uint32_t> mUsed;
  vector<Edge> aResult;
  for (const auto& edge : aEdges) {
    if (mUsed[edge.strX]&edge.eX || mUsed[edge.strY]&edge.eY) continue;
    mUsed[edge.strX] |= edge.eX;
    mUsed[edge.strY] |= edge.eY;
    aResult.emplace_back(edge);
  }

  return aResult;
}

shared_ptr<ezra::Extension> FindExtension(const string& strReadId, const vector<shared_ptr<Extension>>& aExt) {
  for(auto ext : aExt) {
    if (ext->ext_strName == strReadId) {
      return ext;
    }
  }

  return nullptr;
}

ExtensionType GetSuffixExtensionType(const bool bReverseComplement) {
  return bReverseComplement ? ET_PREFIX : ET_SUFFIX;
}

ExtensionType GetPrefixExtensionType(const bool bReverseComplement) {
  return bReverseComplement ? ET_SUFFIX : ET_PREFIX;
}

void DoBridging(const vector<Edge> &aEdges, MapIdToSeq& mIdToContig,
  MapIdToSeq& mIdToRead,  MapIdToExt& mSeqToExt,
  unordered_set<string>& asUsed, string& strCurrentId, string& strNewContig,
  bool &bReverseComplement) {

  while(true) {
    const ExtensionType eType = GetSuffixExtensionType(bReverseComplement);
    int iEdge = -1;
    for (uint32_t i = 0; i < aEdges.size(); i++) {
      if ((aEdges[i].strX == strCurrentId && aEdges[i].eX == eType) ||
          (aEdges[i].strY == strCurrentId && aEdges[i].eY == eType)) {
        iEdge = i;
      }
    }
    if (iEdge == -1) return;
    auto pExtCurr = FindExtension(aEdges[iEdge].strBridgeRead, mSeqToExt[{strCurrentId, eType}]);
    const bool bCurrentIsX = aEdges[iEdge].strX == strCurrentId;
    // determine next contig id
    const string strNext = bCurrentIsX ? aEdges[iEdge].strY : aEdges[iEdge].strX;
    if (!asUsed.insert(strNext).second) return;

    auto pExtNext = FindExtension(aEdges[iEdge].strBridgeRead, mSeqToExt[{strNext, bCurrentIsX ? aEdges[iEdge].eY : aEdges[iEdge].eX}]);

    // bridge 2 contigs
    strNewContig = strNewContig.substr(0, strNewContig.size() - pExtCurr->ext_ulTargetDelta + 1);
    const uint32_t ulQBegin = min(pExtCurr->ext_ulQBegin, pExtNext->ext_ulQBegin);
    const uint32_t ulQEnd = max(pExtCurr->ext_ulQEnd, pExtNext->ext_ulQEnd);
    string strBridge = mIdToRead[aEdges[iEdge].strBridgeRead]->seq_strData.substr(ulQBegin, ulQEnd - ulQBegin + 1);

    // if bridge and contig are in different strand, do reverse complement on bridge
    if ((pExtCurr->ext_bOrientation && bReverseComplement) || (!pExtCurr->ext_bOrientation && !bReverseComplement)) {
      strBridge = _bioReverseComplement(strBridge);
    }

    strNewContig += strBridge;
    string strContigY = mIdToContig[strNext]->seq_strData;
    if (pExtNext->ext_eType == ET_PREFIX) {
      strContigY = strContigY.substr(pExtNext->ext_ulTEnd);
      bReverseComplement = false;
    } else {
      strContigY = _bioReverseComplement(strContigY.substr(0, pExtNext->ext_ulTBegin));
      bReverseComplement = true;
    }

    strNewContig += strContigY;
    strCurrentId = strNext;
  }
}

uint32_t GetMaxDelta(const vector<shared_ptr<ezra::Extension>>& aExt) {
  uint32_t ulMaxDelta = 0;
  for (const auto pExt : aExt) {
    ulMaxDelta = max(ulMaxDelta, pExt->ext_ulTargetDelta);
  }
  return ulMaxDelta;
}

string TruncateExtension(const string& strExtension, const ExtensionType eType, const vector<uint32_t>& aCoverage,
  uint32_t ulMaxCoverage, uint32_t ulMinLength) {
  const uint32_t ulSize = aCoverage.size();
  if (ulSize == 0) return strExtension;
  const uint32_t ulThreshold = ulMaxCoverage / 2;
  if (eType == ET_SUFFIX) {
    // truncate suffix from the end
    uint32_t ulPos = ulSize;
    while(aCoverage[--ulPos] < ulThreshold && ulPos > 0 && ulPos > ulMinLength);
    return strExtension.substr(0, ulPos);
  } else {
    // truncate prefix from beginning
    uint32_t ulPos = 0;
    while(++ulPos < ulSize && aCoverage[ulPos] < ulThreshold && ulSize - ulPos > ulMinLength);
    return strExtension.substr(ulPos);
  }
}

uint32_t CreateExtension(const string& strContigId, const MapIdToSeq& mIdToContig,
  const MapIdToSeq& mIdToRead, vector<shared_ptr<ezra::Extension>> aExt, string& strConsensus) {
  const uint32_t ulExtensionThreshold = 3;
  if (aExt.size() < ulExtensionThreshold) {
    return 0;
  }

  const ExtensionType eExtType = aExt[0]->ext_eType;
  auto alignment_engine = spoa::createAlignmentEngine(static_cast<spoa::AlignmentType>(0), 5, -4, -8);
  auto graph = spoa::createGraph();

  // align the target contig first
  const uint32_t ulExtDelta = GetMaxDelta(aExt);
  const auto itCtg = mIdToContig.find(strContigId);
  const string strContigData = itCtg->second->seq_strData;
  const string strTarget = eExtType == ET_PREFIX ?
    strContigData.substr(0, ulExtDelta) : strContigData.substr(strContigData.size() - ulExtDelta);
  auto alignment = alignment_engine->align_sequence_with_graph(strTarget, graph);
  graph->add_alignment(alignment, strTarget);

  // sort extensions by their position on the target contig
  sort(aExt.begin(), aExt.end(),
    [](const shared_ptr<ezra::Extension>& lhs, const shared_ptr<ezra::Extension>& rhs) {
      return lhs->ext_ulTBegin < rhs->ext_ulTBegin;
    }
  );

  for (const auto pext : aExt) {
    uint32_t ulBegin, ulEnd;
    const auto itRead = mIdToRead.find(pext->ext_strName);
    pext->GetExtensionPosition(ulBegin, ulEnd);
    string strExt = itRead->second->seq_strData.substr(ulBegin, ulEnd - ulBegin + 1);
    if (!pext->ext_bOrientation) {
      strExt = ezra::_bioReverseComplement(strExt);
    }
    auto alignment = alignment_engine->align_sequence_with_graph(strExt, graph);
    graph->add_alignment(alignment, strExt);
  }

  vector<uint32_t> aCoverage;
  strConsensus = graph->generate_consensus(aCoverage, false);
  // cut off the consensus when the coverage drops
  strConsensus = TruncateExtension(strConsensus, eExtType, aCoverage, aExt.size(), ulExtDelta);

  cerr << StringFromExtensionType(eExtType) << " " << strContigId << " consensus " << strConsensus.size()
        << " bp from " << aExt.size() << " reads" << " original len " << aCoverage.size() << " min length " << ulExtDelta << endl;

  return ulExtDelta;
}


void ConsumeEdges(const vector<Edge> &aEdges, MapIdToSeq& mIdToContig,
  MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt) {
  	if (multithreading == 0) {
  		cerr << "INFO: consuming edges in a single thread!" << endl;
  		ConsumeEdgesST(aEdges, mIdToContig, mIdToRead, mSeqToExt);
  	}
  	else {
  		cerr << "INFO: consuming edges using multiple threads!" << endl;
  		ConsumeEdgesMT(aEdges, mIdToContig, mIdToRead, mSeqToExt);
  	}
}


// KK: Extends a contig and returns a new sequence
std::pair<std::string, std::string> ExtendContig(ScaffoldExtension sExtension, MapIdToSeq& mIdToContig, MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt){

  	string strNewContig = sExtension.seq;

    // if this contig is not circular, extended
    const ExtensionType eSuffixExtType = GetSuffixExtensionType(sExtension.bRCSuffix);
    const ExtensionType ePrefixExtType = GetPrefixExtensionType(sExtension.bRCPrefix);
    const bool bIsCircular = CalculateBridgeCoverage(mSeqToExt[{sExtension.strSfxId, eSuffixExtType}],
      mSeqToExt[{sExtension.strPfxId, ePrefixExtType}], eSuffixExtType != ePrefixExtType).size() > 0;
    if (!bIsCircular) {
      cerr << "INFO: Extending non-circular contig: " << sExtension.name << endl;
      // extend suffix
      cerr << "INFO: Extending suffix for: " << sExtension.name << endl;
      string strConsensus;
      uint32_t ulExtDelta = CreateExtension(sExtension.strSfxId, mIdToContig, mIdToRead, mSeqToExt[{sExtension.strSfxId, eSuffixExtType}], strConsensus);
      if (ulExtDelta > 0) {
        if (sExtension.bRCSuffix) {
          strConsensus = _bioReverseComplement(strConsensus);
        }
        strNewContig = strNewContig.substr(0, strNewContig.size() - ulExtDelta) + strConsensus;
      }
      // extend prefix
      cerr << "INFO: Extending prefix for: " << sExtension.name << endl;
      strConsensus = "";
      ulExtDelta = CreateExtension(sExtension.strPfxId, mIdToContig, mIdToRead, mSeqToExt[{sExtension.strPfxId, ePrefixExtType}], strConsensus);
      if (ulExtDelta > 0) {
        if (sExtension.bRCPrefix) {
          strConsensus = _bioReverseComplement(strConsensus);
        }
        strNewContig = strConsensus + strNewContig.substr(ulExtDelta);
      }
    }

    cerr << "INFO: Finished extending for contig: " << sExtension.name << endl;
    return std::make_pair(sExtension.name, strNewContig);
}


// KK: A multi-threaded version of consume edges
//     Using one thread per contig
void ConsumeEdgesMT(const vector<Edge> &aEdges, MapIdToSeq& mIdToContig,
  MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt) {

  // KK: First do bridging without multithreading
  unordered_set<string> asUsed;
  vector<ScaffoldExtension> aPartialScaffolds;

  // Brigdging
  for (const auto& contig : mIdToContig) {
  	// If the contig is already used, continue
    if (!asUsed.insert(contig.first).second) continue;

    string strCurrentId = contig.first;
    string strNewContig = contig.second->seq_strData;
    bool bReverseComplement = false;
    DoBridging(aEdges, mIdToContig, mIdToRead, mSeqToExt, asUsed, strCurrentId, strNewContig, bReverseComplement);
    // remember suffix data
    const string strSuffixId = strCurrentId;
    const bool bReverseComplementSuffix = bReverseComplement;

    strCurrentId = contig.first;
    strNewContig = _bioReverseComplement(strNewContig);
    bReverseComplement = true;
    DoBridging(aEdges, mIdToContig, mIdToRead, mSeqToExt, asUsed, strCurrentId, strNewContig, bReverseComplement);
    strNewContig = _bioReverseComplement(strNewContig);

    // remember prefix data
    const string strPrefixId = strCurrentId;
    const bool bReverseComplementPrefix = !bReverseComplement;

    aPartialScaffolds.emplace_back(contig.first, strNewContig, bReverseComplementSuffix, strSuffixId, bReverseComplementPrefix, strPrefixId);
  }

  // Extending contigs, one thread per contig
  // Using: ExtendContig(ScaffoldExtension sExtension, MapIdToSeq& mIdToContig, MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt)
  
  // create thread pool
  std::shared_ptr<thread_pool::ThreadPool> thread_pool = thread_pool::createThreadPool(ezra::multithreading);
  
  // create storage for return values, functions are returning a pair od strings
  std::vector<std::future<std::pair<std::string, std::string>>> thread_futures;
  
  for (const auto& ps : aPartialScaffolds) {
  	cerr << "INFO: Extending contig " << ps.name << " in a separate thread!" << endl;
  	thread_futures.emplace_back(thread_pool->submit_task(ExtendContig, ps, std::ref(mIdToContig), std::ref(mIdToRead), std::ref(mSeqToExt)));
  }

  // wait for threads to finish
  for (auto& it: thread_futures) {
    it.wait();
    // get return value with it.get();
  }

  // Getting the results
  for (auto& tfuture : thread_futures) {
    if (! tfuture.valid()) {
      cerr << "ERROR: invalide future!" << endl;
    }
    std::pair<std::string, std::string> spair = tfuture.get();
    cout << ">" << spair.first << endl;
    cout << spair.second << endl;
  }
}

// KK: A single threaded version of consume edges
void ConsumeEdgesST(const vector<Edge> &aEdges, MapIdToSeq& mIdToContig,
  MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt) {
  unordered_set<string> asUsed;

  for (const auto& contig : mIdToContig) {
  	// If the contig is already used, continue
    if (!asUsed.insert(contig.first).second) continue;

    string strCurrentId = contig.first;
    string strNewContig = contig.second->seq_strData;
    cerr << "INFO: consuming edges for contig:" << strCurrentId << endl;
    cerr << "INFO: Bridging ..." << endl;
    bool bReverseComplement = false;
    DoBridging(aEdges, mIdToContig, mIdToRead, mSeqToExt, asUsed, strCurrentId, strNewContig, bReverseComplement);
    // remember suffix data
    const string strSuffixId = strCurrentId;
    const bool bReverseComplementSuffix = bReverseComplement;

    strCurrentId = contig.first;
    strNewContig = _bioReverseComplement(strNewContig);
    bReverseComplement = true;
    DoBridging(aEdges, mIdToContig, mIdToRead, mSeqToExt, asUsed, strCurrentId, strNewContig, bReverseComplement);
    strNewContig = _bioReverseComplement(strNewContig);

    // remember prefix data
    const string strPrefixId = strCurrentId;
    const bool bReverseComplementPrefix = !bReverseComplement;

    // if this contig is not circular, extended
    cerr << "INFO: Extending ...";
    const ExtensionType eSuffixExtType = GetSuffixExtensionType(bReverseComplementSuffix);
    const ExtensionType ePrefixExtType = GetPrefixExtensionType(bReverseComplementPrefix);
    const bool bIsCircular = CalculateBridgeCoverage(mSeqToExt[{strSuffixId, eSuffixExtType}],
      mSeqToExt[{strPrefixId, ePrefixExtType}], eSuffixExtType != ePrefixExtType).size() > 0;
    if (!bIsCircular) {
      // extend suffix
      cerr << " suffix ...";
      string strConsensus;
      uint32_t ulExtDelta = CreateExtension(strSuffixId, mIdToContig, mIdToRead, mSeqToExt[{strSuffixId, eSuffixExtType}], strConsensus);
      if (ulExtDelta > 0) {
        if (bReverseComplementSuffix) {
          strConsensus = _bioReverseComplement(strConsensus);
        }
        strNewContig = strNewContig.substr(0, strNewContig.size() - ulExtDelta) + strConsensus;
      }
      // extend prefix
      cerr << " prefix ..." << endl;
      strConsensus = "";
      ulExtDelta = CreateExtension(strPrefixId, mIdToContig, mIdToRead, mSeqToExt[{strPrefixId, ePrefixExtType}], strConsensus);
      if (ulExtDelta > 0) {
        if (bReverseComplementPrefix) {
          strConsensus = _bioReverseComplement(strConsensus);
        }
        strNewContig = strConsensus + strNewContig.substr(ulExtDelta);
      }
    }

    cout << ">" << contig.first << endl;
    cout << strNewContig << endl;
  }
}

}
