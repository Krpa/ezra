#pragma once

#include "Types.h"
#include <string>
#include "Extension.h"

namespace ezra {

struct Edge {
  std::string strX; // contig x
  std::string strY; // contig y
  ExtensionType eX;
  ExtensionType eY;
  uint32_t ulCoverage;
  std::string strBridgeRead;

  Edge(std::string strX, std::string strY, ExtensionType eX, ExtensionType eY, const std::vector<std::string>& aBridge);
};

struct ScaffoldExtension {
	std::string name;
	std::string seq;
	bool bRCSuffix;
	std::string strSfxId;
	bool bRCPrefix;
	std::string strPfxId;

	ScaffoldExtension(std::string name, std::string seq, bool bRCSuffix,
	                  std::string strSfxId, bool bRCPrefix, std::string strPfxId);
};

extern std::vector<Edge> CreateChains(const std::vector<std::string>& aContigIds, MapIdToExt& mSeqToExt);

extern void ConsumeEdges(const std::vector<Edge> &aEdges, MapIdToSeq& mIdToContig,
  MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt);

void ConsumeEdgesST(const std::vector<Edge> &aEdges, MapIdToSeq& mIdToContig,
  MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt);

void ConsumeEdgesPart(const std::vector<Edge> &aEdges, MapIdToSeq& mIdToContig,
  MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt);

void ConsumeEdgesMT(const std::vector<Edge> &aEdges, MapIdToSeq& mIdToContig,
  MapIdToSeq& mIdToRead, MapIdToExt& mSeqToExt);
}
