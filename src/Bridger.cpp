#include "Bridger.h"
#include "Extension.h"
#include "Parser.h"
#include "Graph.h"
#include <vector>
#include <string>
#include <iostream>

namespace ezra {

using namespace std;

extern int multithreading;
extern int chunksize;

Bridger::Bridger(const string& strReads, const string& strPaf, const string& strContigs) {
  Initialize(strReads, strPaf, strContigs);
}

void Bridger::Initialize(const string& strReads, const string& strPaf, const string& strContigs) {
    SetRName readsUsed;
    cerr << "INFO: Loading contigs!" << endl;
    parseProcessFasta(strContigs, mIdToContig);
    cerr << "INFO: Contigs loades:" << mIdToContig.size() << endl;
    cerr << "INFO: Loading overlaps!" << endl;
    if (chunksize > 0) parseProcessPafExt(strPaf, mIdToExt, readsUsed);
    else parseProcessPafExt(strPaf, mIdToExt);
    cerr << "INFO: Overlaps use " << readsUsed.size() << " reads!" << endl;
    cerr << "INFO: Loading reads!" << endl;
    if (chunksize > 0) parseProcessFastq(strReads, mIdToRead, readsUsed);	// Load reads last, ignoring those that are not used in overlaps
    else parseProcessFastq(strReads, mIdToRead);
    cerr << "INFO: Reads loaded: " << mIdToRead.size() << endl;
}

void Bridger::Execute(void) {

  /* KK: For testing multithreading
  if (multithreading == 1){
  	cerr << endl << "WARNING: Using multithreading!" << endl;
  }
  else {
  	cerr << endl << "WARNING: Running program in a single thread!" << endl;
  }
  */

  vector<string> aContigIds;
  for(auto& it : mIdToContig) {
    aContigIds.emplace_back(it.first);
  }
  vector<ezra::Edge> aEdges = ezra::CreateChains(aContigIds, mIdToExt);
  for (const auto& edge : aEdges) {
    cerr << "Bridge: " << edge.strX << " " << edge.eX << " "<< edge.strY << " " << edge.eY << " "<< edge.ulCoverage << endl;
  }

  ConsumeEdges(aEdges, mIdToContig, mIdToRead, mIdToExt);
}

}
