# EZRA

Mentor: prof.dr.sc. Mile Šikić
Thesis: Scaffolding Assembled Genomes with Long Reads

Ezra is a c++ implementation of algorithms presented in my master's thesis.

## Dependencies
1. cmake 3.5+

## Installation
To build EZRA run the following commands:

```bash
git clone --recursive git@gitlab.com:Krpa/Diplomski.git ezra
cd ezra
mkdir build
cd build
cmake ..
make
```

## Usage
The example usage is written in src/main.cpp, output is written to stdout. The main executable is: <ezra instalation folder>/buid/ezra'.
